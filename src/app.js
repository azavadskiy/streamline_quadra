var http = require('http');
var host = process.env.QUADRA_SLUG;
if (!host){
    host = process.env.HOSTNAME;
}
var server = http.createServer(function (request, response) {
      response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Hello from "+host+"\n");
});
server.listen(80);
console.log("Server running");
