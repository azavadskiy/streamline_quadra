all:
		@echo "######################################################################"
		@echo "Streamline PaaS demo directory \n"
		@echo "######################################################################"

docs:
		@echo "######################################################################"
		@echo "Visit https://docs.quadra.opendns.com/#running-qq-for-the-first-time"
		@echo "######################################################################"

install-quake:
		@echo "######################################################################"
		@echo "Quake is a tool to manage deployments in Quadra"
		@echo "######################################################################"
		curl -s https://get.quadra.io/install | bash

deploy:
		@echo "######################################################################"
		@echo "Building image. Deployment file live in .quake/demo.toml"
		@echo "######################################################################"
		quake demo build
		quake demo up 

stop:
		@echo "######################################################################"
		@echo "Stopping demo pool. deployment file live in .quake/demo.toml"
		@echo "######################################################################"
		quake demo stop

destroy:
		@echo "######################################################################"
		@echo "Destroying demo pool. deployment file live in .quake/demo.toml"
		@echo "######################################################################"
		quake demo delete
